const { randomBytes } = require("crypto");

class Token {
  userID;
  token;
  constructor(userID, token = randomBytes(64).toString("base64")) {
    this.userID = userID;
    this.token = token;
  }
}
module.exports = Token;
