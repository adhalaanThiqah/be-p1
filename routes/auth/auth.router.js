// TODO: Create a route that will do the following:
// 1. Handle a POST request to /auth/login that will take in an email and password as the request body
//      and will return a JSON object with a token property. This token SHOULD be stored in the database.
// 2. Handle a POST request to /auth/profile that will take in a token in the request header with key Authentication.
//      Our clients should send the token in the following format: "Bearer <token>". for example:
//      "Bearer 1234567890". If the token is valid, then return a JSON object with the user's profile.
const express = require("express");
const HttpError = require("../../models/http-error.model");
const authService = require("../../services/auth.service");

const router = express.Router();

const login = (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({
      message: "Bad Request",
    });
  }
  try {
    const token = authService.auth(email, password);
    res.status(200).send(token);
  } catch (error) {
    throw new HttpError(error.message);
  }
};
const profile = (req, res) => {
  const token = req.get("Bearer");
  if (!token) {
    return res.status(400).json({
      message: "Bad Request",
    });
  }
  try {
    const user = authService.getUserProfile(token);
    res.status(200).send(user);
  } catch (error) {
    throw new HttpError(error.message);
  }
};

router.post("/login", login);
router.post("/profile", profile);

module.exports = router;
