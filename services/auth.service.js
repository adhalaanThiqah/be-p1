// TODO: Create auth service that will do the following:
// 1. Create a method that will take in an email and password; based on the email,
//      find the user in the database and compare the password. If the password is correct, then
//      generate a token and store it in the database. If the user already owns a token in
//      the database we have to delete the previous token before creating a new one. and then we have to return the token.

// 2. Create a method that will take in a token and return the user's who owns the token.
const tokens = require("../databases/tokens");
const Token = require("../models/token.model");
const { usersService } = require("../services/users.service");

class AuthService {
  database;
  userService;
  constructor(database, userService) {
    this.database = database;
    this.userService = userService;
  }

  _generateToken(id) {
    const index = this.database.findIndex(function (tkn) {
      return tkn.userID == id;
    });
    if (index != -1) this.database.splice(index, 1);

    const token = new Token(id);
    this.database.push(token);

    return token;
  }

  auth(email, password) {
    try {
      const user = this.userService.findUserByEmail(email);
      if (user.password == password) return this._generateToken(user.id);

      throw new Error("Passwords dont match");
    } catch (error) {
      throw new Error(error);
    }
  }
  _getUserId(token) {
    const uT = this.database.find(function (res) {
      return res.token == token;
    });
    if (!uT) {
      throw new Error("Token doesnt exists");
    }
    return uT.userID;
  }
  getUserProfile(token) {
    try {
      const id = this._getUserId(token);
      return this.userService.findOneOrFail(id);
    } catch (error) {
      throw new Error(error);
    }
  }
}
const authService = new AuthService(tokens, usersService);

module.exports = authService;
